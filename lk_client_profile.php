<?php include("template/header-lk.php") ?>

<div class="section-lk-top">
    <div class="container">
        <div class="top">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 p-l p-r">
                    <div class="name-person">
                        <div class="all">
                            <h4>Профиль пользователя</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-4 col-sm-3">
                    <div class="but-btn but-lk">
                        <a href="">Сохранить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content-form-profile">
            <div class="col-sm-12">
                <div class="profile-form">
                    <form id="lk_profile" class="form-group form-all f-prof">
                        <input type="hidden" name="val" value="Задать вопрос">
                        <div class="recourse-lk">
                            <div class="item">
                                <p>Обращение:</p>
                            </div>
                            <div class="recourse-name">
                                <div class="item">
                                    <label>
                                        <input class="radio" type="radio" name="radio-test" value="Господин" checked>
                                        <span class="radio-custom"></span>
                                        <span class="label">Господин</span>
                                    </label>
                                </div>
                                <div class="item">
                                    <label>
                                        <input class="radio" type="radio" name="radio-test" value="Госпожа">
                                        <span class="radio-custom"></span>
                                        <span class="label">Госпожа</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="center-input">
                            <div class="block-profile">
                                <div class="v-d">
                                    <input class="popup-call__name" type="text" name="name" placeholder="Имя">
                                </div>
                                <div class="v-d">
                                    <input class="popup-call__name" type="text" name="surname" placeholder="Фамилия">
                                </div>
                            </div>
                            <label class="all-label">Адрес:</label>
                            <div class="block-profile">
                                <div class="adress-pr">
                                    <div class="v-d-index">
                                        <input class="index" type="text" name="index" placeholder="Индекс" maxlength="5">
                                    </div>
                                    <div class="v-d">
                                        <input class="city" type="text" name="city" placeholder="Город">
                                    </div>
                                </div>
                                <div class="v-d">
                                    <input class="adress-form" type="text" name="adress" placeholder="Адрес">
                                </div>
                            </div>
                            <label class="all-label">Контактные данные:</label>
                            <div class="block-profile">
                                <div class="v-d">
                                    <input class="popup-call__email" type="tel" name="email" placeholder="Email">
                                </div>
                                <div class="v-d">
                                    <input class="form-control" placeholder="Телефон"
                                           name="phone" id="Piramida_phone-qust" type="tel" maxlength="20"/>
                                    <div class="help-block error" id="Piramida_phone_em_" style="display:none"></div>
                                    <input type="checkbox" id="phone_mask-qust"
                                           placeholder="Телефон" checked>
                                    <label id="descr" for="phone_mask2" class="phone-mask2">Использовать маску</label>
                                </div>
                            </div>
                            <label class="all-label">Изменить пароль:</label>
                            <div class="block-profile">
                                <div class="v-d">
                                    <input id="password1" type="password" name="password_new" placeholder="Новый пароль">
                                </div>
                                <div class="v-d">
                                    <input id="password2" type="password" name="password_repeat" placeholder="Повторить пароль">
                                </div>
                            </div>
                            <p id="validate-status"></p>
                            <div class="profile-but">
                                <button class="hover-el">Сохранить пароль</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>
