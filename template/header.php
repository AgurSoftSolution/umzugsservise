<!DOCTYPE html>
<head lang="ru">
    <meta charset="UTF-8">
    <title>Umzugsunternehmen</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="/fonts/fonts.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/plugin/fancybox-master/dist/jquery.fancybox.min.css">
    <!-- OwlCarousel  -->
    <link rel="stylesheet" type="text/css" href="/plugin/OwlCarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/plugin/OwlCarousel/dist/assets/owl.theme.default.min.css">
    <!-- OwlCarousel  -->

    <link rel="stylesheet" type="text/css" href="/plugin/aSendForm/assets/a-valid.css">
    <link rel="stylesheet" type="text/css" href="/css/template.css">
    <link rel="stylesheet" type="text/less" href="/css/main.less">
    <link rel="stylesheet" type="text/css" href="/css/animate.css">
    <link rel="stylesheet" type="text/css" href="/css/media.css">
    <link rel="stylesheet" type="text/css" href="/css/mediaWork.css">
    <link rel="stylesheet" type="text/css" href="/css/mediaPartners.css">
    <link rel="stylesheet" type="text/css" href="/css/mediaAboutUs.css">
    <link rel="stylesheet" type="text/css" href="/css/mediaFaq.css">
    <link rel="stylesheet" type="text/css" href="/css/mediaBlog.css">
    <link rel="stylesheet" type="text/css" href="/css/lk.css">
    <link rel="stylesheet" type="text/css" href="/css/mediaBlogCity.css">
    <link rel="stylesheet" type="text/css" href="/css/a-valid.css">

    <!-- AOS  -->
    <link rel="stylesheet" href="/plugin/aos-master/dist/aos.css">
    <!-- AOS  -->

    <!-- woocommerce  -->
    <link rel="stylesheet" type="text/css" href="/plugin/woocommerce/flexslider.css"/>
    <!-- woocommerce  -->

    <link rel="stylesheet" href="/plugin/jquery-ui/jquery-datepicker-skins-master/css/jquery-ui-1.10.1.css">
    <link rel="stylesheet" href="/plugin/jquery-ui/jquery-datepicker-skins-master/css/cangas.datepicker.css">
    <link rel="stylesheet" href="/plugin/jquery-ui/jquery-datepicker-skins-master/css/vigo.datepicker.css">
    <script type="text/javascript" src="/plugin/Jquery.min/jquery.min.js"></script>
    <script type="text/javascript" src="/plugin/Bootstrap/bootstrap.min.js"></script>

    <!-- fancybox-master-->
    <script type="text/javascript" src="/plugin/fancybox-master/dist/jquery.fancybox.min.js"></script>
    <!-- fancybox-master-->

    <!-- owl-->
    <script type="text/javascript" src="/plugin/OwlCarousel/dist/owl.carousel.min.js"></script>
    <!-- owl-->

 <!-- mustache-->
      <script type="text/javascript" src="/plugin/mustache.js-master/mustache.min.js"></script>
    <!-- mustache->

   <!-- mask -->
    <script type="text/javascript" src="/plugin/inputmask/js/inputmask.js"></script>
    <script type="text/javascript" src="/plugin/inputmask/js/inputmask-multi.js"></script>
    <script type="text/javascript" src="/plugin/inputmask/js/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/plugin/inputmask/js/inputmask.phone.js"></script>
    <!-- mask -->

    <!-- less-->
    <script type="text/javascript" src="/plugin/less/dist/less.min.js"></script>
    <!-- less-->

    <!-- ui-->
    <script type="text/javascript" src="/plugin/jquery-ui/jquery-datepicker-skins-master/js/jquery-ui-1.10.1.min.js"></script>
    <!-- ui-->

    <!--jquery.spincrement-->
    <script type="text/javascript"  src="/plugin/jquery.spincrement/jquery.spincrement.min.js"></script>
    <!-- jquery.spincrement-->

    <script src="http://maps.google.com/maps/api/js?key=AIzaSyAFx_cIAOZ6jhkql93Yi6emV6vsrV_cxd8"></script>

    <!-- woocommerce  -->
    <script type="text/javascript" src="/plugin/woocommerce/jquery.flexslider.js"></script>
    <!-- woocommerce  -->

    <!-- gmaps-->
    <script type="text/javascript" src="/plugin/gmap/dist/gmap3.min.js"></script>
    <!-- gmaps-->

    <!-- aSentForm -->
    <script type="text/javascript" src="/plugin/aSendForm/assets/jquery.send.form.min.js"></script>
    <script type="text/javascript" src="/plugin/aSendForm/assets/jquery.mask.min.js"></script>
    <script type="text/javascript" src="/plugin/aSendForm/assets/jquery.bpopup.min.js"></script>
    <script type="text/javascript" src="/plugin/aSendForm/assets/handle.php"></script>
    <!-- aSentForm -->

    <!-- aos-->
    <script type="text/javascript" src="/plugin/aos-master/dist/aos.js"></script>
    <!-- aos-->
    <script type='text/javascript' src="/plugin/aSendForm/assets/jquery.send.form.js"></script>
    <script type='text/javascript' src="/plugin/aSendForm/assets/jquery.validate.min.js"></script>
    <script type='text/javascript' src="/plugin/aSendForm/assets/additional-methods.min.js"></script>
    <script type="text/javascript" src="/js/all.js"></script>
    <script type="text/javascript" src="/js/validate.js"></script>
</head>
<!--<script>-->
<!---->
<!--    var view = {-->
<!--        name : "Joe",-->
<!--        occupation : "Web Developer"-->
<!--    };-->
<!--    function loadtemp(){-->
<!--        var output = Mustache.render("{{name}} is a  {{occupation}}", view);-->
<!--        document.getElementById('person').innerHTML = output;-->
<!--    }-->
<!--</script>-->
<body>

<header>
