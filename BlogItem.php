<?php include("template/header-main.php") ?>

<div class="section-break item-blog">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="break-menu">
                    <a class="back" href="/">Главная </a><span>|</span>
                    <a class="back" href="/blog.php">Блог </a><span>|</span>
                    <p>Статья</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-descript-article">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Длинное название статьи о том, что надо знать любому человеку для успешного переезда</h2>
                    <div class="icon">
                        <div class="calendar">
                            <p>дд.мм.гггг</p>
                        </div>
                        <div class="section">
                            <a href="/blog.php">Раздел 1</a>
                        </div>
                    </div>
                    <div class="foto">
                        <img src="img/BlogArticle/foto1.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-constructor">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="constructor">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget.</p>
                    <h3>Heading 3</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget.</p>
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                        bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                        sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                        ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                        rhoncus pronin sapien nunc accuan eget.</p>
                    <h4>Heading 4</h4>
                    <ul>
                        <li><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></li>
                        <li><span>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</span>
                        </li>
                        <li><span>Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span>
                        </li>
                        <li><span>Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span>
                        </li>
                    </ul>
                    <ol>
                        <li><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></li>
                        <li><span>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</span>
                        </li>
                        <li><span>Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span>
                        </li>
                        <li><span>Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span>
                        </li>
                    </ol>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget.</p>

                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                        bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                        sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                        ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                        rhoncus pronin sapien nunc accuan eget.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="share">
                    <p>Поделиться:</p>
                    <div class="pluso" data-background="#ebebeb"
                         data-options="medium,round,line,horizontal,counter,theme=04"
                         data-services="twitter,facebook,google,linkedin,pinterest"></div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="section-helpful-information articleBlog">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Похожие статьи</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="relocation repeat-col useful-blog">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto1.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Длинное название статьи о том, что надо знать любому человеку для успешного
                            переезда</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="relocation repeat-col useful-blog">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto2.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Длинное название статьи о том, что надо знать любому человеку для успешного
                            переезда</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="relocation repeat-col useful-blog">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto3.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Длинное название статьи о том, что надо знать любому человеку для успешного
                            переезда</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/BlogCity.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>
