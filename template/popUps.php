<div id="js-document" class="footer-doc">
    <div class="title">
        <h2>Заголовок документации</h2>
    </div>
    <div class="document">
        <h4>1. Leistungen</h4>
        <p>Der Möbelspediteur erbringt seine Verpflichtung mit der größten Sorgfalt und unter Wahrung des Interesses des
            Absenders gegen Zahlung des vereinbarten Entgelts. Entstehen im Rahmen der vertraglichen Leistung
            unvorhersehbare Aufwendungen, sind diese durch den Auftraggeber zu ersetzen, sofern sie der Möbelspediteur
            den Umständen nach für erforderlich halten durfte. Erweitert der Absender nach Vertragsschluss den
            Leistungsumfang, sind die hierdurch entstandenen Mehrkosten in angemessener Höhe zu vergüten. Das Personal
            des Möbelspediteurs ist, sofern nichts anderes vereinbart ist, nicht zur Vornahme von Elektro-, Gas-, Dübel-
            und sonstigen Installationsarbeiten berechtigt.</p>
        <h4>2. Beiladungstransport</h4>
        <p>Der Umzug darf auch als Beiladungstransport durchgeführt werden.
            Beauftragung Dritter
            Der Möbelspediteur kann einen weiteren Frachtführer mit der Durchführung des Umzugs
            beauftragen.</p>
        <h4>3. Erstattung der Umzugskosten</h4>
        <p>Soweit der Absender gegenüber einem Dritten einen Anspruch auf Umzugskostenvergütung hat, weist er diesen an,
            die vereinbarte und fällige Umzugskostenvergütung abzüglich geleisteter Anzahlungen oder Teilzahlungen auf
            entsprechende Anforderung direkt an den Möbelspediteur zu zahlen.

            Transportsicherungen/Hinweispflicht des Absenders

            Der Absender ist verpflichtet, bewegliche oder elektronische Teile, insbesondere an empfindlichen Geräten,
            fachgerecht für den Transport sichern zu lassen. Zur Überprüfung der fachgerechten Transportsicherung ist
            der Möbelspediteur nicht verpflichtet. Zählt zu dem Umzugsgut gefährliches Gut, ist der Absender
            verpflichtet, dem Möbelspediteur rechtzeitig anzugeben, welcher Natur die Gefahr ist, die von dem Gut
            ausgeht.

            Aufrechnung

            Gegen Ansprüche des Möbelspediteurs ist eine Aufrechnung nur mit fälligen Gegenansprüchen zulässig, die
            rechtskräftig festgestellt, entscheidungsreif oder unbestritten sind.

            Weisungen und Mitteilungen

            Weisungen und Mitteilungen des Absenders bezüglich der Durchführung der Beförderung sind in Textform
            ausschließlich an den Auftragnehmer zu richten.

            Nachprüfung durch den Absender

            Bei Abholung des Umzugsgutes ist der Absender verpflichtet nachzuprüfen, dass kein Gegenstand irrtümlich
            mitgenommen oder stehengelassen wird.

            Fälligkeit des vereinbarten Entgelts

            Der Rechnungsbetrag ist, sofern vertraglich nicht anderes vereinbart wurde, bei Inlandstransporten nach
            Beendigung der Ablieferung, bei Auslandstransporten vor Beginn der Verladung fällig und in bar oder durch
            vorherige Überweisung auf das Geschäftskonto des Möbelspediteurs zu bezahlen.Auslagen in ausländischer
            Währung werden nach dem am Zahlungstag festgestellten Wechselkurs abgerechnet. Kommt der Absender seiner
            Zahlungsverpflichtung nicht nach, ist der Möbelspediteur berechtigt, das Umzugsgut anzuhalten oder nach
            Beginn der Beförderung auf Kosten des Absenders, bis zur Zahlung der Fracht und der bis zu diesem Zeitpunkt
            entstandenen Aufwendungen einzulagern. Kommt der Absender seiner Zahlungsverpflichtung auch dann nicht nach,
            ist der Möbelspediteur berechtigt, eine Pfandverwertung nach den gesetzlichen Vorschriften durchzuführen. §
            419 HGB findet entsprechende Anwendung.

            Trinkgelder

            Trinkgelder werden nicht auf den Rechnungsbetrag angerechnet.

            Rücktritt und Kündigung

            Beim Umzug handelt es sich um eine Dienstleistung im Sinne von § 312 g Absatz 2 Satz 1 Nummer 9 BGB. Es
            besteht kein gesetzliches Widerrufsrecht nach § 355 BGB. Der Absender kann den Umzugsvertrag jederzeit
            kündigen. Kündigt der Absender, so kann der Möbelspediteur, sofern die Kündigung auf Gründen beruht, die
            nicht seinem Risikobereich zuzurechnen sind, entweder die vereinbarte Fracht, das etwaige Standgeld sowie zu
            ersetzende Aufwendungen verlangen. Auf diesen Betrag wird angerechnet, was er infolge der Aufhebung des
            Vertrages an Aufwendungen erspart oder anderweitig erwirbt oder böswillig zu erwerben unterlässt oder
            pauschal ein Drittel der vereinbarten Fracht verlangen.

            Gerichtsstand

            Für Rechtsstreitigkeiten mit Vollkaufleuten auf Grund dieses Vertrages und über Ansprüche aus anderen
            Rechtsgründen, die mit dem Umzugsvertrag zusammenhängen, ist das Gericht, in dessen Bezirk sich die vom
            Absender beauftragte Niederlassung des Möbelspediteurs befindet, ausschließlich zuständig. Für
            Rechtsstreitigkeiten mit anderen als Vollkaufleuten gilt die ausschließliche Zuständigkeit nur für den Fall,
            dass der Absender nach Vertragsabschluss seinen Wohnsitz oder gewöhnlichen Aufenthaltsort in das Ausland
            verlegt oder sein Wohnsitz oder persönlichen Aufenthaltsort zum Zeitpunkt der Klageerhebung nicht bekannt
            ist.

            Rechtswahl

            Es gilt deutsches Recht.

            Datenschutz

            Der Möbelspediteur verwendet die vom Kunden mitgeteilten Daten zur Erfüllung und Abwicklung des Auftrages.
            Eine Weitergabe der Daten erfolgt an Erfüllungsgehilfen, soweit diese zur Auftragserfüllung eingesetzt
            werden. Eine Weitergabe der Daten an sonstige Dritte erfolgt nicht. Mit vollständiger Abwicklung des
            Auftrages und vollständiger Bezahlung werden die Daten für die weitere Verwendung gesperrt und nach Ablauf
            der steuer- und handelsrechtlichen Vorschriften gelöscht.</p>
    </div>
</div>

<div id="js-impressum" class="footer-doc">
    <div class="title">
        <h2>Заголовок документации</h2>
    </div>
    <div class="document">
        <h4>1. Leistungen</h4>
        <p>Der Möbelspediteur erbringt seine Verpflichtung mit der größten Sorgfalt und unter Wahrung des Interesses des
            Absenders gegen Zahlung des vereinbarten Entgelts. Entstehen im Rahmen der vertraglichen Leistung
            unvorhersehbare Aufwendungen, sind diese durch den Auftraggeber zu ersetzen, sofern sie der Möbelspediteur
            den Umständen nach für erforderlich halten durfte. Erweitert der Absender nach Vertragsschluss den
            Leistungsumfang, sind die hierdurch entstandenen Mehrkosten in angemessener Höhe zu vergüten. Das Personal
            des Möbelspediteurs ist, sofern nichts anderes vereinbart ist, nicht zur Vornahme von Elektro-, Gas-, Dübel-
            und sonstigen Installationsarbeiten berechtigt.</p>
        <h4>2. Beiladungstransport</h4>
        <p>Der Umzug darf auch als Beiladungstransport durchgeführt werden.
            Beauftragung Dritter
            Der Möbelspediteur kann einen weiteren Frachtführer mit der Durchführung des Umzugs
            beauftragen.</p>
        <h4>3. Erstattung der Umzugskosten</h4>
        <p>Soweit der Absender gegenüber einem Dritten einen Anspruch auf Umzugskostenvergütung hat, weist er diesen an,
            die vereinbarte und fällige Umzugskostenvergütung abzüglich geleisteter Anzahlungen oder Teilzahlungen auf
            entsprechende Anforderung direkt an den Möbelspediteur zu zahlen.

            Transportsicherungen/Hinweispflicht des Absenders

            Der Absender ist verpflichtet, bewegliche oder elektronische Teile, insbesondere an empfindlichen Geräten,
            fachgerecht für den Transport sichern zu lassen. Zur Überprüfung der fachgerechten Transportsicherung ist
            der Möbelspediteur nicht verpflichtet. Zählt zu dem Umzugsgut gefährliches Gut, ist der Absender
            verpflichtet, dem Möbelspediteur rechtzeitig anzugeben, welcher Natur die Gefahr ist, die von dem Gut
            ausgeht.

            Aufrechnung

            Gegen Ansprüche des Möbelspediteurs ist eine Aufrechnung nur mit fälligen Gegenansprüchen zulässig, die
            rechtskräftig festgestellt, entscheidungsreif oder unbestritten sind.

            Weisungen und Mitteilungen

            Weisungen und Mitteilungen des Absenders bezüglich der Durchführung der Beförderung sind in Textform
            ausschließlich an den Auftragnehmer zu richten.

            Nachprüfung durch den Absender

            Bei Abholung des Umzugsgutes ist der Absender verpflichtet nachzuprüfen, dass kein Gegenstand irrtümlich
            mitgenommen oder stehengelassen wird.

            Fälligkeit des vereinbarten Entgelts

            Der Rechnungsbetrag ist, sofern vertraglich nicht anderes vereinbart wurde, bei Inlandstransporten nach
            Beendigung der Ablieferung, bei Auslandstransporten vor Beginn der Verladung fällig und in bar oder durch
            vorherige Überweisung auf das Geschäftskonto des Möbelspediteurs zu bezahlen.Auslagen in ausländischer
            Währung werden nach dem am Zahlungstag festgestellten Wechselkurs abgerechnet. Kommt der Absender seiner
            Zahlungsverpflichtung nicht nach, ist der Möbelspediteur berechtigt, das Umzugsgut anzuhalten oder nach
            Beginn der Beförderung auf Kosten des Absenders, bis zur Zahlung der Fracht und der bis zu diesem Zeitpunkt
            entstandenen Aufwendungen einzulagern. Kommt der Absender seiner Zahlungsverpflichtung auch dann nicht nach,
            ist der Möbelspediteur berechtigt, eine Pfandverwertung nach den gesetzlichen Vorschriften durchzuführen. §
            419 HGB findet entsprechende Anwendung.

            Trinkgelder

            Trinkgelder werden nicht auf den Rechnungsbetrag angerechnet.

            Rücktritt und Kündigung

            Beim Umzug handelt es sich um eine Dienstleistung im Sinne von § 312 g Absatz 2 Satz 1 Nummer 9 BGB. Es
            besteht kein gesetzliches Widerrufsrecht nach § 355 BGB. Der Absender kann den Umzugsvertrag jederzeit
            kündigen. Kündigt der Absender, so kann der Möbelspediteur, sofern die Kündigung auf Gründen beruht, die
            nicht seinem Risikobereich zuzurechnen sind, entweder die vereinbarte Fracht, das etwaige Standgeld sowie zu
            ersetzende Aufwendungen verlangen. Auf diesen Betrag wird angerechnet, was er infolge der Aufhebung des
            Vertrages an Aufwendungen erspart oder anderweitig erwirbt oder böswillig zu erwerben unterlässt oder
            pauschal ein Drittel der vereinbarten Fracht verlangen.

            Gerichtsstand

            Für Rechtsstreitigkeiten mit Vollkaufleuten auf Grund dieses Vertrages und über Ansprüche aus anderen
            Rechtsgründen, die mit dem Umzugsvertrag zusammenhängen, ist das Gericht, in dessen Bezirk sich die vom
            Absender beauftragte Niederlassung des Möbelspediteurs befindet, ausschließlich zuständig. Für
            Rechtsstreitigkeiten mit anderen als Vollkaufleuten gilt die ausschließliche Zuständigkeit nur für den Fall,
            dass der Absender nach Vertragsabschluss seinen Wohnsitz oder gewöhnlichen Aufenthaltsort in das Ausland
            verlegt oder sein Wohnsitz oder persönlichen Aufenthaltsort zum Zeitpunkt der Klageerhebung nicht bekannt
            ist.

            Rechtswahl

            Es gilt deutsches Recht.

            Datenschutz

            Der Möbelspediteur verwendet die vom Kunden mitgeteilten Daten zur Erfüllung und Abwicklung des Auftrages.
            Eine Weitergabe der Daten erfolgt an Erfüllungsgehilfen, soweit diese zur Auftragserfüllung eingesetzt
            werden. Eine Weitergabe der Daten an sonstige Dritte erfolgt nicht. Mit vollständiger Abwicklung des
            Auftrages und vollständiger Bezahlung werden die Daten für die weitere Verwendung gesperrt und nach Ablauf
            der steuer- und handelsrechtlichen Vorschriften gelöscht.</p>
    </div>
</div>


<div id="js-datenschutz" class="footer-doc">
    <div class="title">
        <h2>Заголовок документации</h2>
    </div>
    <div class="document">
        <h4>1. Leistungen</h4>
        <p>Der Möbelspediteur erbringt seine Verpflichtung mit der größten Sorgfalt und unter Wahrung des Interesses des
            Absenders gegen Zahlung des vereinbarten Entgelts. Entstehen im Rahmen der vertraglichen Leistung
            unvorhersehbare Aufwendungen, sind diese durch den Auftraggeber zu ersetzen, sofern sie der Möbelspediteur
            den Umständen nach für erforderlich halten durfte. Erweitert der Absender nach Vertragsschluss den
            Leistungsumfang, sind die hierdurch entstandenen Mehrkosten in angemessener Höhe zu vergüten. Das Personal
            des Möbelspediteurs ist, sofern nichts anderes vereinbart ist, nicht zur Vornahme von Elektro-, Gas-, Dübel-
            und sonstigen Installationsarbeiten berechtigt.</p>
        <h4>2. Beiladungstransport</h4>
        <p>Der Umzug darf auch als Beiladungstransport durchgeführt werden.
            Beauftragung Dritter
            Der Möbelspediteur kann einen weiteren Frachtführer mit der Durchführung des Umzugs
            beauftragen.</p>
        <h4>3. Erstattung der Umzugskosten</h4>
        <p>Soweit der Absender gegenüber einem Dritten einen Anspruch auf Umzugskostenvergütung hat, weist er diesen an,
            die vereinbarte und fällige Umzugskostenvergütung abzüglich geleisteter Anzahlungen oder Teilzahlungen auf
            entsprechende Anforderung direkt an den Möbelspediteur zu zahlen.

            Transportsicherungen/Hinweispflicht des Absenders

            Der Absender ist verpflichtet, bewegliche oder elektronische Teile, insbesondere an empfindlichen Geräten,
            fachgerecht für den Transport sichern zu lassen. Zur Überprüfung der fachgerechten Transportsicherung ist
            der Möbelspediteur nicht verpflichtet. Zählt zu dem Umzugsgut gefährliches Gut, ist der Absender
            verpflichtet, dem Möbelspediteur rechtzeitig anzugeben, welcher Natur die Gefahr ist, die von dem Gut
            ausgeht.

            Aufrechnung

            Gegen Ansprüche des Möbelspediteurs ist eine Aufrechnung nur mit fälligen Gegenansprüchen zulässig, die
            rechtskräftig festgestellt, entscheidungsreif oder unbestritten sind.

            Weisungen und Mitteilungen

            Weisungen und Mitteilungen des Absenders bezüglich der Durchführung der Beförderung sind in Textform
            ausschließlich an den Auftragnehmer zu richten.

            Nachprüfung durch den Absender

            Bei Abholung des Umzugsgutes ist der Absender verpflichtet nachzuprüfen, dass kein Gegenstand irrtümlich
            mitgenommen oder stehengelassen wird.

            Fälligkeit des vereinbarten Entgelts

            Der Rechnungsbetrag ist, sofern vertraglich nicht anderes vereinbart wurde, bei Inlandstransporten nach
            Beendigung der Ablieferung, bei Auslandstransporten vor Beginn der Verladung fällig und in bar oder durch
            vorherige Überweisung auf das Geschäftskonto des Möbelspediteurs zu bezahlen.Auslagen in ausländischer
            Währung werden nach dem am Zahlungstag festgestellten Wechselkurs abgerechnet. Kommt der Absender seiner
            Zahlungsverpflichtung nicht nach, ist der Möbelspediteur berechtigt, das Umzugsgut anzuhalten oder nach
            Beginn der Beförderung auf Kosten des Absenders, bis zur Zahlung der Fracht und der bis zu diesem Zeitpunkt
            entstandenen Aufwendungen einzulagern. Kommt der Absender seiner Zahlungsverpflichtung auch dann nicht nach,
            ist der Möbelspediteur berechtigt, eine Pfandverwertung nach den gesetzlichen Vorschriften durchzuführen. §
            419 HGB findet entsprechende Anwendung.

            Trinkgelder

            Trinkgelder werden nicht auf den Rechnungsbetrag angerechnet.

            Rücktritt und Kündigung

            Beim Umzug handelt es sich um eine Dienstleistung im Sinne von § 312 g Absatz 2 Satz 1 Nummer 9 BGB. Es
            besteht kein gesetzliches Widerrufsrecht nach § 355 BGB. Der Absender kann den Umzugsvertrag jederzeit
            kündigen. Kündigt der Absender, so kann der Möbelspediteur, sofern die Kündigung auf Gründen beruht, die
            nicht seinem Risikobereich zuzurechnen sind, entweder die vereinbarte Fracht, das etwaige Standgeld sowie zu
            ersetzende Aufwendungen verlangen. Auf diesen Betrag wird angerechnet, was er infolge der Aufhebung des
            Vertrages an Aufwendungen erspart oder anderweitig erwirbt oder böswillig zu erwerben unterlässt oder
            pauschal ein Drittel der vereinbarten Fracht verlangen.

            Gerichtsstand

            Für Rechtsstreitigkeiten mit Vollkaufleuten auf Grund dieses Vertrages und über Ansprüche aus anderen
            Rechtsgründen, die mit dem Umzugsvertrag zusammenhängen, ist das Gericht, in dessen Bezirk sich die vom
            Absender beauftragte Niederlassung des Möbelspediteurs befindet, ausschließlich zuständig. Für
            Rechtsstreitigkeiten mit anderen als Vollkaufleuten gilt die ausschließliche Zuständigkeit nur für den Fall,
            dass der Absender nach Vertragsabschluss seinen Wohnsitz oder gewöhnlichen Aufenthaltsort in das Ausland
            verlegt oder sein Wohnsitz oder persönlichen Aufenthaltsort zum Zeitpunkt der Klageerhebung nicht bekannt
            ist.

            Rechtswahl

            Es gilt deutsches Recht.

            Datenschutz

            Der Möbelspediteur verwendet die vom Kunden mitgeteilten Daten zur Erfüllung und Abwicklung des Auftrages.
            Eine Weitergabe der Daten erfolgt an Erfüllungsgehilfen, soweit diese zur Auftragserfüllung eingesetzt
            werden. Eine Weitergabe der Daten an sonstige Dritte erfolgt nicht. Mit vollständiger Abwicklung des
            Auftrages und vollständiger Bezahlung werden die Daten für die weitere Verwendung gesperrt und nach Ablauf
            der steuer- und handelsrechtlichen Vorschriften gelöscht.</p>
    </div>

</div>


<div id="js-agb" class="footer-doc">
    <div class="title">
        <h2>Заголовок документации</h2>
    </div>
    <div class="document">
        <h4>1. Leistungen</h4>
        <p>Der Möbelspediteur erbringt seine Verpflichtung mit der größten Sorgfalt und unter Wahrung des Interesses des
            Absenders gegen Zahlung des vereinbarten Entgelts. Entstehen im Rahmen der vertraglichen Leistung
            unvorhersehbare Aufwendungen, sind diese durch den Auftraggeber zu ersetzen, sofern sie der Möbelspediteur
            den Umständen nach für erforderlich halten durfte. Erweitert der Absender nach Vertragsschluss den
            Leistungsumfang, sind die hierdurch entstandenen Mehrkosten in angemessener Höhe zu vergüten. Das Personal
            des Möbelspediteurs ist, sofern nichts anderes vereinbart ist, nicht zur Vornahme von Elektro-, Gas-, Dübel-
            und sonstigen Installationsarbeiten berechtigt.</p>
        <h4>2. Beiladungstransport</h4>
        <p>Der Umzug darf auch als Beiladungstransport durchgeführt werden.
            Beauftragung Dritter
            Der Möbelspediteur kann einen weiteren Frachtführer mit der Durchführung des Umzugs
            beauftragen.</p>
        <h4>3. Erstattung der Umzugskosten</h4>
        <p>Soweit der Absender gegenüber einem Dritten einen Anspruch auf Umzugskostenvergütung hat, weist er diesen an,
            die vereinbarte und fällige Umzugskostenvergütung abzüglich geleisteter Anzahlungen oder Teilzahlungen auf
            entsprechende Anforderung direkt an den Möbelspediteur zu zahlen.

            Transportsicherungen/Hinweispflicht des Absenders

            Der Absender ist verpflichtet, bewegliche oder elektronische Teile, insbesondere an empfindlichen Geräten,
            fachgerecht für den Transport sichern zu lassen. Zur Überprüfung der fachgerechten Transportsicherung ist
            der Möbelspediteur nicht verpflichtet. Zählt zu dem Umzugsgut gefährliches Gut, ist der Absender
            verpflichtet, dem Möbelspediteur rechtzeitig anzugeben, welcher Natur die Gefahr ist, die von dem Gut
            ausgeht.

            Aufrechnung

            Gegen Ansprüche des Möbelspediteurs ist eine Aufrechnung nur mit fälligen Gegenansprüchen zulässig, die
            rechtskräftig festgestellt, entscheidungsreif oder unbestritten sind.

            Weisungen und Mitteilungen

            Weisungen und Mitteilungen des Absenders bezüglich der Durchführung der Beförderung sind in Textform
            ausschließlich an den Auftragnehmer zu richten.

            Nachprüfung durch den Absender

            Bei Abholung des Umzugsgutes ist der Absender verpflichtet nachzuprüfen, dass kein Gegenstand irrtümlich
            mitgenommen oder stehengelassen wird.

            Fälligkeit des vereinbarten Entgelts

            Der Rechnungsbetrag ist, sofern vertraglich nicht anderes vereinbart wurde, bei Inlandstransporten nach
            Beendigung der Ablieferung, bei Auslandstransporten vor Beginn der Verladung fällig und in bar oder durch
            vorherige Überweisung auf das Geschäftskonto des Möbelspediteurs zu bezahlen.Auslagen in ausländischer
            Währung werden nach dem am Zahlungstag festgestellten Wechselkurs abgerechnet. Kommt der Absender seiner
            Zahlungsverpflichtung nicht nach, ist der Möbelspediteur berechtigt, das Umzugsgut anzuhalten oder nach
            Beginn der Beförderung auf Kosten des Absenders, bis zur Zahlung der Fracht und der bis zu diesem Zeitpunkt
            entstandenen Aufwendungen einzulagern. Kommt der Absender seiner Zahlungsverpflichtung auch dann nicht nach,
            ist der Möbelspediteur berechtigt, eine Pfandverwertung nach den gesetzlichen Vorschriften durchzuführen. §
            419 HGB findet entsprechende Anwendung.

            Trinkgelder

            Trinkgelder werden nicht auf den Rechnungsbetrag angerechnet.

            Rücktritt und Kündigung

            Beim Umzug handelt es sich um eine Dienstleistung im Sinne von § 312 g Absatz 2 Satz 1 Nummer 9 BGB. Es
            besteht kein gesetzliches Widerrufsrecht nach § 355 BGB. Der Absender kann den Umzugsvertrag jederzeit
            kündigen. Kündigt der Absender, so kann der Möbelspediteur, sofern die Kündigung auf Gründen beruht, die
            nicht seinem Risikobereich zuzurechnen sind, entweder die vereinbarte Fracht, das etwaige Standgeld sowie zu
            ersetzende Aufwendungen verlangen. Auf diesen Betrag wird angerechnet, was er infolge der Aufhebung des
            Vertrages an Aufwendungen erspart oder anderweitig erwirbt oder böswillig zu erwerben unterlässt oder
            pauschal ein Drittel der vereinbarten Fracht verlangen.

            Gerichtsstand

            Für Rechtsstreitigkeiten mit Vollkaufleuten auf Grund dieses Vertrages und über Ansprüche aus anderen
            Rechtsgründen, die mit dem Umzugsvertrag zusammenhängen, ist das Gericht, in dessen Bezirk sich die vom
            Absender beauftragte Niederlassung des Möbelspediteurs befindet, ausschließlich zuständig. Für
            Rechtsstreitigkeiten mit anderen als Vollkaufleuten gilt die ausschließliche Zuständigkeit nur für den Fall,
            dass der Absender nach Vertragsabschluss seinen Wohnsitz oder gewöhnlichen Aufenthaltsort in das Ausland
            verlegt oder sein Wohnsitz oder persönlichen Aufenthaltsort zum Zeitpunkt der Klageerhebung nicht bekannt
            ist.

            Rechtswahl

            Es gilt deutsches Recht.

            Datenschutz

            Der Möbelspediteur verwendet die vom Kunden mitgeteilten Daten zur Erfüllung und Abwicklung des Auftrages.
            Eine Weitergabe der Daten erfolgt an Erfüllungsgehilfen, soweit diese zur Auftragserfüllung eingesetzt
            werden. Eine Weitergabe der Daten an sonstige Dritte erfolgt nicht. Mit vollständiger Abwicklung des
            Auftrages und vollständiger Bezahlung werden die Daten für die weitere Verwendung gesperrt und nach Ablauf
            der steuer- und handelsrechtlichen Vorschriften gelöscht.</p>
    </div>
</div>
<div id="js-partner" class="partner-popup">
    <div class="title-partner">
        <h2>Стать партнером</h2>
    </div>
    <div class="block-info">
        <p>Для получения подробной информации заполните форму и наш менеджер свяжется с Вами в ближайшее время</p>
        <form id="popup-partner" class="form-all form-group">
            <input type="hidden" name="val" value="Стать партнером">
            <div class="recourse">
                <div class="item">
                    <p>Обращение</p>
                </div>
                <div class="recourse-name">
                    <div class="item">
                        <label>
                            <input class="radio" type="radio" name="radio-test" value="Господин" checked>
                            <span class="radio-custom"></span>
                            <span class="label">Господин</span>
                        </label>
                    </div>
                    <div class="item">
                        <label>
                            <input class="radio" type="radio" name="radio-test" value="Госпожа">
                            <span class="radio-custom"></span>
                            <span class="label">Госпожа</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="center-input">
                <input class="popup-call__name" type="text" name="name" placeholder="Ваше имя">
                    <input class="form-control"  placeholder="Телефон" name="phone" id="Piramida_phone" type="tel" maxlength="20" />
                    <div class="help-block error" id="Piramida_phone_em_" style="display:none"></div>
                <input type="checkbox" id="phone_mask" placeholder="Телефон" checked>
                <label id="descr" for="phone_mask" class="phone-mask">Использовать маску</label>
                <input class="popup-call__email" type="tel" name="email" placeholder="Email">
                <textarea class="popup-picking__text" rows="3" name="commit"
                          placeholder="Текст комментария"></textarea>
                <div class="form-popup">
                    <button class="exploreButton framedButton form-popup">Отправить</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="js-qustion" class="partner-popup">
    <div class="title-partner">
        <h2>Задать вопрос</h2>
    </div>
    <div class="block-info">
        <p>Для получения подробной информации заполните форму и наш менеджер свяжется с Вами в ближайшее время</p>
        <form id="popup-qustion" class="form-group form-all">
            <input type="hidden" name="val" value="Задать вопрос">
            <div class="recourse">
                <div class="item">
                    <p>Обращение</p>
                </div>
                <div class="recourse-name">
                    <div class="item">
                        <label>
                            <input class="radio" type="radio" name="radio-test" value="Господин" checked>
                            <span class="radio-custom"></span>
                            <span class="label">Господин</span>
                        </label>
                    </div>
                    <div class="item">
                        <label>
                            <input class="radio" type="radio" name="radio-test" value="Госпожа">
                            <span class="radio-custom"></span>
                            <span class="label">Госпожа</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="center-input">
                <input class="popup-call__name" type="text" name="name" placeholder="Ваше имя">
                <input class="form-control"  placeholder="Телефон" name="phone" id="Piramida_phone-qust" type="tel" maxlength="20" />
                <div class="help-block error" id="Piramida_phone_em_" style="display:none"></div>
                <input type="checkbox" id="phone_mask-qust"  placeholder="Телефон" checked>
                <label id="descr" for="phone_mask2" class="phone-mask2">Использовать маску</label>
                <input class="popup-call__email" type="tel" name="email" placeholder="Email">
                <textarea class="popup-picking__text" rows="3" name="commit"
                          placeholder="Ваш вопрос"></textarea>
                <div class="form-popup">
                    <button class="exploreButton framedButton form-popup">Отправить</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div id="js_popup_slider_foto" class="popup-clider-foto">
    <div class="gallery-sliders">
        <div   id="slider" class="flexslider">
            <ul class="slides">
                <li>
                    <img src="img/order-relocation/tabs3/foto-slider.png" alt="" />
                </li>
                <li>
                    <img src="img/order-relocation/tabs3/foto-slider.png" alt="" />
                </li>
                <li>
                    <img src="img/order-relocation/tabs3/foto-slider.png" alt="" />
                </li>
                <li>
                    <img src="img/order-relocation/tabs3/foto-slider.png" alt="" />
                </li>
                <li>
                    <img src="img/order-relocation/tabs3/foto-slider.png" alt="" />
                </li>
            </ul>
        </div>
        <div  id="carousel" class="js-gallery-carousel flexslider down_foto">
            <ul class="slides">
                <li>
                    <img src="img/order-relocation/tabs3/lit-foto-slider.png" alt="" />
                </li>
                <li>
                    <img src="img/order-relocation/tabs3/lit-foto2.png" alt="" />
                </li>
                <li>
                    <img src="img/order-relocation/tabs3/lit-foto-slider.png" alt="" />
                </li>
                <li>
                    <img src="img/main/block8/foto2.png" alt="" />
                </li>
                <li>
                    <img src="img/order-relocation/tabs3/1.jpg" alt="" />
                </li>

            </ul>
        </div>
    </div>
</div>
<div id="answer">
    <div class="answer-title">
        <h2>Ваша заявка принята</h2>
        <p>Мы свяжемся с Вами в ближайше время</p>
    </div>
</div>


