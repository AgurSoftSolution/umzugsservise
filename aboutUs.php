<?php include("template/header-main.php") ?>

<div class="section-aboutUs-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2 data-aos="zoom-out" data-aos-delay="100" data-aos-once="true">Effektiv Service</h2>
                    <div class="line"></div>
                    <div data-aos="zoom-out" data-aos-delay="100" data-aos-once="true" class="main-info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                            sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                            mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus
                            pronin sapien nunc accuan eget.
                        </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                            sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                            mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus
                            pronin sapien nunc accuan eget.</p>
                    </div>
                </div>
                <div class="car" data-aos="fade-right"
                     data-aos-offset="100" data-aos-duration="800"
                     data-aos-easing="ease-in-sine" data-aos-delay="200" data-aos-once="true">
                    <img src="img/aboutUs/block1/car2.png" alt="">
                </div>
                <div class="right-pict" data-aos="fade-left"
                     data-aos-offset="100" data-aos-duration="800"
                     data-aos-easing="ease-in-sine" data-aos-delay="200" data-aos-once="true">
                    <img src="img/aboutUs/block1/right-foto.png" alt="">
                </div>
            </div>
            <div class="mob_car">
                <img src="img/aboutUs/block1/car2.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="section-goal">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="block-goal">
                    <div class="block-title">
                        <div class="title">
                            <h4>Наша цель</h4>
                        </div>
                        <div class="line"></div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-command">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Наша команда</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="ass-block" data-aos="zoom-in"
                 data-aos-offset="100" data-aos-duration="800"
                 data-aos-easing="ease-in-sine" data-aos-delay="100" data-aos-once="true">
                <div class="col-sm-6">
                    <div class="command">
                        <div class="foto-associate">
                            <div class="foto">
                                <img src="img/aboutUs/block3/foto1.png" alt="">
                            </div>
                            <div class="imposition">
                            </div>
                        </div>
                        <div class="info-associate">
                            <h3>Вильгельм Гогенцоллерн</h3>
                            <h4>Должность</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar
                                sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                                mus. Nam fermentum, nulla luctus pharetra vulputate, felis
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="command">
                        <div class="foto-associate">
                            <div class="foto">
                                <img src="img/aboutUs/block3/foto2.png" alt="">
                            </div>
                            <div class="imposition">
                            </div>
                        </div>
                        <div class="info-associate">
                            <h3>Фридрихом Вельдерфальтерфильтергрому </h3>
                            <h4>Должность</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ass-block" data-aos="zoom-in"
                 data-aos-offset="100" data-aos-duration="800"
                 data-aos-easing="ease-in-sine" data-aos-delay="800" data-aos-once="true">
                <div class="col-sm-6">
                    <div class="command">
                        <div class="foto-associate">
                            <div class="foto">
                                <img src="img/aboutUs/block3/foto3.png" alt="">
                            </div>
                            <div class="imposition">
                            </div>
                        </div>
                        <div class="info-associate">
                            <h3>Вернер Хартманн</h3>
                            <h4>Должность</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar
                                sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                                mus. Nam fermentum, nulla luctus pharetra vulputate
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="command">
                        <div class="foto-associate">
                            <div class="foto">
                                <img src="img/partners/block1/man2.png" alt="">
                            </div>
                            <div class="imposition">
                            </div>
                        </div>
                        <div class="info-associate">
                            <h3>Вильгельм шульц </h3>
                            <h4>Должность</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet.
                                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales
                                pulvinar
                                sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                                mus. Nam fermentum, nulla luctus pharetra vulputate
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ass-block">
            </div>
        </div>
    </div>
</div>
<div class="section-contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Наши контакты</h2>
                </div>
                <div class="block-map">
                    <div class="contacs">
                        <h3>Effektiv Service</h3>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/aboutUs/block4/map-placeholder.svg" alt="">
                            </div>
                            <div class="info">
                                <p>Düsternortstraße 85
                                    27755 Delmenhorst</p>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/aboutUs/block4/mail.svg" alt="">
                            </div>
                            <div class="info">
                                <a href="mailto:mail@email.com">mail@email.com</a>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/aboutUs/block4/telephone.svg" alt="">
                            </div>
                            <div class="info">
                                <a href="tel:+042213959800">04221 - 3959800</a>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/aboutUs/block4/fax.svg" alt="">
                            </div>
                            <div class="info">
                                <a href="tel:+042213959801">04221 - 3959801</a>
                            </div>
                        </div>
                        <div class="all-block">
                            <div class="icon">
                                <img src="img/aboutUs/block4/clock.svg" alt="">
                            </div>
                            <div class="info">
                                <p>Mo - Do: 08:00 - 18:00 <br> Fr: 0800 - 1700</p>
                            </div>
                        </div>
                    </div>
                    <div id="map_adress"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-issues">
    <div class="container">
        <div class="row">
            <div class="issues">
                <div class="left-col" data-aos="zoom-in-right" data-aos-delay="100" data-aos-once="true">
                    <img src="img/aboutUs/block5/foto-girl.png" alt="">
                </div>
                <div class="right-col">
                    <h3 data-aos="zoom-in-left" data-aos-delay="500" data-aos-once="true">Если у вас возникли вопросы,
                        задайте их нашему специалисту</h3>
                    <div class="btn-question b-all">
                        <a href="#js-qustion">Задать вопрос</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/map.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>
