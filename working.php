<?php include("template/header-main.php") ?>

<div class="section-how-working">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Как мы работаем</h2>
                    <div class="list-work">
                        <div class="list">
                            <div class="number">1</div>
                            <p>Просто</p>
                        </div>
                        <div class="list">
                            <div class="number">2</div>
                            <p>Удобно</p>
                        </div>
                        <div class="list">
                            <div class="number">3</div>
                            <p>Надежно</p>
                        </div>
                    </div>
                </div>
                <div class="mob_working" data-aos="fade-right"
                     data-aos-offset="100" data-aos-duration="800"
                     data-aos-easing="ease-in-sine" data-aos-delay="200">
                    <div class="left-pict" data-aos="fade-right"
                         data-aos-offset="100" data-aos-duration="800"
                         data-aos-easing="ease-in-sine" data-aos-delay="200">
                    </div>
                </div>
                <div class="mob_man">
                    <img src="img/work/block1/man.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-list-work">
    <div class="section-slider-working owl-carousel">
        <div class="item " data-dot="1">
            <div class="left-col">
                <div class="item-info">
                    <div class="descript">
                        <h2>Заполнить заявку</h2>
                        <ul>
                            <li>что делать</li>
                            <li>объем работ</li>
                            <li>контактные данные</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="right-col application wow fadeInRight" data-wow-delay="1s" data-wow-duraction="2s">
                <div class="application"></div>
            </div>
        </div>
        <div class="item" data-dot="2">
            <div class="left-col">
                <div class="item-info">
                    <div class="descript">
                        <h2>
                            Получить предложения</h2>
                        <ul>
                            <li>количество предложений не ограничено</li>
                            <li>компании предлагают стоимость выполнения услуги</li>
                            <li>возможность запросить стоимость у избранных компаний</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="right-col">
                <div class="sentence"></div>
            </div>
        </div>
        <div class="item" data-dot="3">
            <div class="left-col">
                <div class="item-info">
                    <div class="descript">
                        <h2>Выбрать исполнителя</h2>
                        <ul>
                            <li>по стоимости</li>
                            <li>по отзывам</li>
                            <li>по качеству обслуживания</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="right-col">
                <div class="executor"></div>
            </div>
        </div>
        <div class="item" data-dot="4">
            <div class="left-col">
                <div class="item-info">
                    <div class="descript">
                        <h2>Выполнение услуги и оплата</h2>
                        <ul>
                            <li>никаких скрытых платежей</li>
                            <li>оплата после выполнения услуги</li>
                            <li>варианты оплаты: наличными, по счету, онлайн-оплата</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="right-col">
                <div class="services">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-technologies">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="descript-technologies">
                    <div class="info">
                        <p>При современных быстрых темпах изменения окружающего мира, сильным остается не тот, кто
                            рвется единолично вперед, а тот, кто действует сообща. Так как сила - в единстве.</p>
                        <p>И именно поэтому сервис ........ создан с целью объединения возможностей более 300 компаний
                            партнеров для улучшения качества предоставляемых услуг.</p>
                        <p>Мы считаем, что клиент должен получить услуги наилучшего качества. А партнер - наилучшие
                            условия для предоставления этих услуг.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-different-technologies">
    <div class="container">
        <div class="technologes-block">
            <div class="row">
                <div class="col-md-4 col-sm-6 p-l p-r">
                    <div class="list-technologies">
                        <div class="all-block icon_arrow laptop_icon ">
                            <div class="icon room "></div>
                            <div class="name-techn">
                                <p>Квартирный
                                    переезд</p>
                            </div>
                        </div>
                        <div class="info-techn">
                            <p>Хотя бы раз в жизни, каждый человек получает опыт что такое переезд и все сопутствующие с
                                ним
                                заботы. Не важно, студент вы или семейный человек. Не важно что вам надо перевези,
                                десяток
                                коробок или содержимое двухэтажного семейного поместья с множеством старинных
                                антикварных
                                вещей и домашнего добра. Вам в любом случае понадобится помощь. Доверьте это
                                профессионалам.</p>
                            <img class="check_tech" src="img/work/block3/6_chekmark_active.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 p-l p-r">
                    <div class="list-technologies">
                        <div class="all-block  icon_arrow laptop_icon">
                            <div class="icon office "></div>
                            <div class="name-techn">
                                <p>Офисный
                                    переезд</p>
                            </div>
                        </div>
                        <div class="info-techn">
                            <p>Хотя бы раз в жизни, каждый человек получает опыт что такое переезд и все сопутствующие с
                                ним
                                заботы. Не важно, студент вы или семейный человек. Не важно что вам надо перевези,
                                десяток
                                коробок или содержимое двухэтажного семейного поместья с множеством старинных
                                антикварных
                                вещей и домашнего добра. Вам в любом случае понадобится помощь. Доверьте это
                                профессионалам.</p>
                            <img class="check_tech" src="img/work/block3/6_chekmark_active.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 p-l p-r">
                    <div class="list-technologies">
                        <div class="all-block  icon_arrow laptop_icon">
                            <div class="icon other-city"></div>
                            <div class="name-techn">
                                <p>Переезд в другой город</p>
                            </div>
                        </div>
                        <div class="info-techn">
                            <p>Хотя бы раз в жизни, каждый человек получает опыт что такое переезд и все сопутствующие с
                                ним
                                заботы. Не важно, студент вы или семейный человек. Не важно что вам надо перевези,
                                десяток
                                коробок или содержимое двухэтажного семейного поместья с множеством старинных
                                антикварных
                                вещей и домашнего добра. Вам в любом случае понадобится помощь. Доверьте это
                                профессионалам.</p>
                            <img class="check_tech" src="img/work/block3/6_chekmark_active.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 p-l p-r">
                    <div class="list-technologies">
                        <div class="all-block icon_arrow  laptop_icon">
                            <div class="icon other-country"></div>
                            <div class="name-techn">
                                <p>Переезд в другую страну</p>
                            </div>
                        </div>
                        <div class="info-techn">
                            <p>Хотя бы раз в жизни, каждый человек получает опыт что такое переезд и все сопутствующие с
                                ним
                                заботы. Не важно, студент вы или семейный человек. Не важно что вам надо перевези,
                                десяток
                                коробок или содержимое двухэтажного семейного поместья с множеством старинных
                                антикварных
                                вещей и домашнего добра. Вам в любом случае понадобится помощь. Доверьте это
                                профессионалам.</p>
                            <img class="check_tech" src="img/work/block3/6_chekmark_active.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 p-l p-r">
                    <div class="list-technologies">
                        <div class="all-block  icon_arrow laptop_icon">
                            <div class="icon service"></div>
                            <div class="name-techn">
                                <p> Сервис доставки</p>
                            </div>
                        </div>
                        <div class="info-techn">
                            <p>Хотя бы раз в жизни, каждый человек получает опыт что такое переезд и все сопутствующие с
                                ним
                                заботы. Не важно, студент вы или семейный человек. Не важно что вам надо перевези,
                                десяток
                                коробок или содержимое двухэтажного семейного поместья с множеством старинных
                                антикварных
                                вещей и домашнего добра. Вам в любом случае понадобится помощь. Доверьте это
                                профессионалам.</p>
                            <img class="check_tech" src="img/work/block3/6_chekmark_active.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 p-l p-r">
                    <div class="list-technologies">
                        <div class="all-block  icon_arrow laptop_icon">
                            <div class="icon things"></div>
                            <div class="name-techn">
                                <p>Хранение вещей</p>
                            </div>
                        </div>
                        <div class="info-techn">
                            <p>Хотя бы раз в жизни, каждый человек получает опыт что такое переезд и все сопутствующие с
                                ним
                                заботы. Не важно, студент вы или семейный человек. Не важно что вам надо перевези,
                                десяток
                                коробок или содержимое двухэтажного семейного поместья с множеством старинных
                                антикварных
                                вещей и домашнего добра. Вам в любом случае понадобится помощь. Доверьте это
                                профессионалам.</p>
                            <img class="check_tech" src="img/work/block3/6_chekmark_active.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-guarantees">
    <div class="left-foto"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="our-guar" data-wow-offset="10" data-aos-once="true">
                </div>
                <div class="block-descript">
                    <h2 data-aos="zoom-out" data-aos-delay="100" data-aos-once="true">Наши гарантии</h2>
                    <div class="list-guarantees">
                        <div class="item" data-aos="zoom-out-left" data-aos-delay="300" data-aos-once="true">
                            <div class="pict certificate"></div>
                            <p>Мы работаем только с проверенными сертифицированными специалистами.</p>
                        </div>
                        <div class="item" data-aos="zoom-out-left" data-aos-delay="600" data-aos-once="true">
                            <div class="pict client"></div>
                            <p>Клиент защищен от недобросовестных предпринимателей — на Effektiv service выполнение
                                заказа
                                гарантировано.</p>
                        </div>
                        <div class="item" data-aos="zoom-out-left" data-aos-delay="900" data-aos-once="true">
                            <div class="pict payment"></div>
                            <p>Никаких срытых платежей, никаких ограничений, вы получаете максимум предложений и выбор
                                исполнителя остается за вами.</p>
                        </div>
                        <div class="item" data-aos="zoom-out-left" data-aos-delay="1200" data-aos-once="true">
                            <div class="pict partner"></div>
                            <p>Наши партнеры готовы к любым условиям переезда. Техническое оснащение позволяет выполнять
                                заказы любого объема и любой сложности.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="found-company">
                    <div class="blue-company"></div>
                    <div class="found-relocation" data-aos="zoom-in-right" data-aos-delay="800" data-aos-once="true">
                        <div class="pict"></div>
                        <h3>Ищете надежную транспортную компанию для переезда?</h3>
                    </div>
                    <div class="btn-offer" data-aos="zoom-out-up" data-aos-delay="1000" data-aos-once="true">
                        <a href="#">Получить предложения</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="section-partners">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="partners-title">
                    <h2>Наши партнеры</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="slider-partners owl-carousel">
                <div class="slider-item">
                    <div class="foto-partners">
                        <img src="img/main/block6/foto-1.png" alt="">
                    </div>
                    <p>Название транспортной компании из Бремена</p>
                </div>
                <div class="slider-item">
                    <div class="foto-partners">
                        <img src="img/main/block6/foto-2.png" alt="">
                    </div>
                    <p>Название транспортной компании из Бремена</p>
                </div>
                <div class="slider-item">
                    <div class="foto-partners">
                        <img src="img/main/block6/foto-3.png" alt="">
                    </div>
                    <p>Название транспортной компании из Бремена</p>
                </div>
                <div class="slider-item">
                    <div class="foto-partners">
                        <img src="img/main/block6/foto-4.png" alt="">
                    </div>
                    <p>Название транспортной компании из Бремена</p>
                </div>
                <div class="slider-item">
                    <div class="foto-partners">
                        <img src="img/main/block6/foto-5.png" alt="">
                    </div>
                    <p>Название транспортной компании из Бремена</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="descript-partners">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
                        Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc
                        eget.</p>
                </div>
                <div class="btn-bottom b-all">
                    <a href="#js-partner">Стать партнером</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-additional-services">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Дополнительные услуги</h2>
                </div>
            </div>
        </div>
        <div class="row" data-aos="fade-up" data-aos-anchor-placement="center-center" data-aos-delay="100"
             data-aos-once="true">
            <div class="col-sm-4">
                <div class="add-s repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto1.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Название услуги</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="add-s repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto2.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Название услуги</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="add-s repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto3.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Длинное название услуги длинное название услуги</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-aos="fade-up" data-aos-anchor-placement="center-center" data-aos-delay="300"
             data-aos-once="true">
            <div class="col-sm-4">
                <div class="add-s repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto4.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Название услуги</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="add-s repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto5.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Длинное название услуги длинное название услуги</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="add-s repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block5/foto4.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Название услуги</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-helpful-information">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>Полезная информация</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="relocation repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block6/foto1.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Рекомендации по организации переезда</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="relocation repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block6/foto2.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Как составить список вещей</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="relocation repeat-col">
                    <div class="foto-relocation">
                        <a href="#">
                            <img src="img/work/block6/foto3.png" alt="">
                        </a>
                    </div>
                    <div class="descript">
                        <a href="#">Как выбрать упаковку</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/work.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>
