<?php include("template/header-lk.php") ?>


<div class="section-lk-top">
    <div class="section-ordering-services">
        <div class="container">
            <div class="row">
                <div class="order">
                    <h3>Заказ услуги</h3>
                    <p>Выберите услугу</p>
                </div>
            </div>
        </div>
    </div>
        <div class="container">
            <div class="section-choce-service">
            <div class="row">
                <div class="col-sm-12">
                    <div class="choce-service">
                        <div class="item relocation" onclick="location.href='/order_move.php';">
                            <div class="pict"></div>
                            <div class="name">
                                <a href="#">
                                    Переезд
                                </a>
                            </div>
                        </div>
                        <div class="item storage" onclick="location.href='/';">
                            <div class="pict"></div>
                            <div class="name">
                                <a href="">Хранение
                                    вещей</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content-news">
            <div class="col-sm-12">
                <div class="news">
                    <h2>
                        Очень длинное название новости
                    </h2>
                    <iframe class="video-news" width="870" height="490" src="https://www.youtube.com/embed/DsTYXUxKpLQ"
                            frameborder="0"
                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    <h3>Heading 3</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget.</p>
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                        bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                        sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                        ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                        rhoncus pronin sapien nunc accuan eget.</p>
                    <h4>Heading 4</h4>
                    <ul>
                        <li><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></li>
                        <li><span>Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</span>
                        </li>
                        <li><span>Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span>
                        </li>
                        <li><span>Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span>
                        </li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget.</p>

                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                        bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                        sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur
                        ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                        rhoncus pronin sapien nunc accuan eget.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>
