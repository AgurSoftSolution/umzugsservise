<?php include("template/header-main.php") ?>

<div class="section-faq-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2 data-aos="zoom-out" data-aos-delay="100" data-aos-once="true">Часто задаваемые вопросы</h2>
                    <div class="line"></div>
                    <div data-aos="zoom-out" data-aos-delay="100" data-aos-once="true" class="main-info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                            sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                            mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus
                            pronin sapien nunc accuan eget.
                        </p>
                    </div>
                </div>
                <div class="girl" data-aos="fade-right"
                     data-aos-offset="100" data-aos-duration="800"
                     data-aos-easing="ease-in-sine" data-aos-delay="200" data-aos-once="true">
                    <img src="img/faq/block1/girl.png" alt="">
                </div>
            </div>
            <div class="mob_girl">
                <img src="img/faq/block1/girl.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="section-qustion-faq">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="some-qustion">
                    <div class="title">
                        <h3>Название раздела вопросов </h3>
                    </div>
                    <div class="tabs_answers">
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="some-qustion">
                    <div class="title">
                        <h3>Название раздела вопросов </h3>
                    </div>
                    <div class="tabs_answers">
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="some-qustion">
                    <div class="title">
                        <h3>Название раздела вопросов </h3>
                    </div>
                    <div class="tabs_answers">
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                        <div class="all">
                            <div class="answer_title hover_title"><h4 href="#">Текст вопроса</h4>
                                <div class="arrow">
                                    <img src="img/partners/block3/6_chekmark.svg" alt="">
                                </div>
                            </div>
                            <div class="answer_info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                    felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-issues">
    <div class="container">
        <div class="row">
            <div class="issues">
                <div class="left-col" data-aos="zoom-in-right" data-aos-delay="100" data-aos-once="true">
                    <img src="img/aboutUs/block5/foto-girl.png" alt="">
                </div>
                <div class="right-col">
                    <h3 data-aos="zoom-in-left" data-aos-delay="500" data-aos-once="true">Если у вас возникли вопросы,
                        задайте их нашему специалисту</h3>
                    <div class="btn-question b-all">
                        <a href="#js-qustion">Задать вопрос</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="/js/faq.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>
