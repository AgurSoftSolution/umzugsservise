<?php include("template/header-lk.php") ?>


<div class="section-lk-top">
    <div class="section-ordering-services">
        <div class="container">
            <div class="row">
                <div class="order-relocation">
                    <h3>Переезд</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myTab" class="section-choice">
    <div class="section-steps-relocation">
        <div class="container">
            <div class="row">
                <div class="steps">
                    <div class="step-menu nav-tabs">
                        <button class="filter active">
                            География переезда
                        </button>
                        <button class="filter">
                            Условия обслуживания
                        </button>

                        <button class="filter">
                            Список вещей
                        </button>

                        <button class="filter not-active-step">
                            Контактные данные
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form>
                    <div class="step-content">
                        <div class="content-choice">
                            <div class="geography-block">
                                <div class="order-lk form-group form-all">
                                    <p>Укажите необходимый адрес перемещения Вашего груза и все сопутствующие параметры
                                        и мы
                                        сможем подобрать для Вас лучшие предложения.</p>
                                    <h4>Откуда</h4>
                                    <div class="all-profile lk-rel">
                                        <div class="adress-lk">
                                            <div class="v-d-index index">
                                                <input class="index" type="text" name="index" placeholder="Индекс"
                                                       maxlength="5">
                                            </div>
                                            <div class="v-d-city">
                                                <input class="city" type="text" name="city" placeholder="Город">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <input class="adress-form" type="text" name="adress" placeholder="Адрес">
                                        </div>
                                    </div>
                                    <h4>Куда</h4>
                                    <div class="all-profile lk-rel">
                                        <div class="adress-lk">
                                            <div class="v-d-index index">
                                                <input class="index" type="text" name="index" placeholder="Индекс"
                                                       maxlength="5">
                                            </div>
                                            <div class="v-d-city">
                                                <input class="city" type="text" name="city" placeholder="Город">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <input class="adress-form" type="text" name="adress" placeholder="Адрес">
                                        </div>
                                    </div>
                                    <div class="line-black"></div>
                                    <div class="all-profile">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Сроки выполнения заказа:</label>
                                                <select class="change_order">
                                                    <option selected>Укажите сроки выполнения заказа</option>
                                                    <option>Срочно</option>
                                                    <option value='1'>Точная дата</option>
                                                    <option value='2'>Диапазон дат</option>
                                                </select>
                                            </div>
                                            <div class="exact-date">
                                                <input class="popup-calendar datepicker_from" name="date_from"
                                                       placeholder="дд.мм.гггг" type="text">
                                            </div>
                                            <div class="date-range">
                                                <input class="popup-calendar datepicker_from" name="date_from"
                                                       placeholder="дд.мм.гггг" type="text">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Площадь помещения:</label>
                                                <input class="adress-form" type="text" name="adress"
                                                       placeholder="Адрес">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="all-profile">
                                        <div class="lk-rel">
                                            <label class="all-label">Способ оплаты:</label>
                                            <select>
                                                <option selected>Укажите кто будет оплачивать заказ</option>
                                                <option>Частное лицо</option>
                                                <option>Государство</option>
                                                <option>Работодатель</option>
                                            </select>
                                        </div>
                                        <div class="lk-rel">
                                            <label class="all-label"> Комментарий к услуге:</label>
                                            <textarea class="popup-picking__text" rows="3" name="commit"
                                                      placeholder="Ваш вопрос"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="but-btn center-but btnNext">
                                    <button>Далее</button>
                                </div>
                            </div>
                        </div>
                        <div class="content-choice two-st">
                            <div class="conditions-block">
                                <div class="order-lk form-group form-all">
                                    <p>Укажите необходимый адрес перемещения Вашего груза и все сопутствующие параметры
                                        и мы
                                        сможем подобрать для Вас лучшие предложения.</p>
                                    <h4>Откуда</h4>
                                    <div class="all-profile lk-rel col-three">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Тип помещения:</label>
                                                <select>
                                                    <option selected>Укажите тип помещения</option>
                                                    <option>Квартира</option>
                                                    <option>Дом</option>
                                                    <option>Комната</option>
                                                    <option>Офис</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Расстояние до парковки, м:</label>
                                                <input class="adress-form" type="text" name="adress"
                                                       placeholder="Расстояние до парковки">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Лифт:</label>
                                                <select>
                                                    <option selected>Укажите наличие лифта</option>
                                                    <option>Есть</option>
                                                    <option>Нет</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Этажей:</label>
                                                <input class="adress-form" type="text" name="adress"
                                                       placeholder="Укажите количество этажей">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Комнат:</label>
                                                <input class="adress-form" type="text" name="adress"
                                                       placeholder="Укажите количество комнат">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Жильцов:</label>
                                                <input class="adress-form" type="text" name="adress"
                                                       placeholder="Укажите количество жильцов">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Необходимость установки знаков временного
                                                    ограничения запрета стоянки транспортных средств:</label>
                                                <select>
                                                    <option selected>Выбрать</option>
                                                    <option>Нет</option>
                                                    <option>Да</option>
                                                    <option>Незнаю</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Куда</h4>
                                    <div class="all-profile lk-rel col-three">
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Тип помещения:</label>
                                                <select>
                                                    <option selected>Укажите тип помещения</option>
                                                    <option>Квартира</option>
                                                    <option>Дом</option>
                                                    <option>Комната</option>
                                                    <option>Офис</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Расстояние до парковки, м:</label>
                                                <input class="adress-form" type="text" name="adress"
                                                       placeholder="Расстояние до парковки">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Лифт:</label>
                                                <select>
                                                    <option selected>Укажите наличие лифта</option>
                                                    <option>Есть</option>
                                                    <option>Нет</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Этажей:</label>
                                                <input class="adress-form" type="text" name="adress"
                                                       placeholder="Укажите количество этажей">
                                            </div>
                                        </div>
                                        <div class="lk-rel">
                                            <div class="r-all">
                                                <label class="all-label">Необходимость установки знаков временного
                                                    ограничения запрета стоянки транспортных средств:</label>
                                                <select>
                                                    <option selected>Выбрать</option>
                                                    <option>Нет</option>
                                                    <option>Да</option>
                                                    <option>Незнаю</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="additional-services">
                                        <h4>Дополнительные услуги</h4>
                                        <div class="checkbox">
                                            <label><input class="add_service" type="checkbox" value="">
                                                <span class="checkmark"></span></label>
                                        </div>
                                    </div>
                                    <p class="add-info">Если необходимы дополнительные услуги, укажите их
                                        пожалуйста.</p>
                                    <div class="hover-service">
                                        <div class="block-add-service">
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Упаковочные коробки</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Демонтаж мебели</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Сборка кухни</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Услуги водопроводчика</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Упаковать вещи</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Сборка мебели</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Столярные работы</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Подъемник</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Распаковать вещи</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Демонтаж кухни</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="">
                                                        <span class="checkmark"></span></label>
                                                </div>
                                                <div class="char">
                                                    <p class="add-info">Услуги электрика</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-serv-descript">
                                        <div class="r-all">
                                            <label class="all-label">Другое:</label>
                                            <textarea class="popup-picking__text" rows="3" name="commit"
                                                      placeholder="Опишите что нужно сделать"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="arrow-steps">
                                    <div class="but-btn btnPrevious">
                                        <button>Назад</button>
                                    </div>
                                    <div class="but-btn  btnNext">
                                        <button>Далее</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-choice three-st">
                            <div class="list-things-block">
                                <div class="form-group form-all choice-block-things">
                                    <h4>Составьте инвентаризацию своих вещей</h4>
                                    <div class="step-things-tree-block"></div>
                                    <p class="js_things_info" Составление списка вещей способствует более точной оценке
                                       стоимости и оптимизации
                                       процесса переезда.</p>
                                    <p class="js_things_info">
                                        Выберите любой удобный способ предоставления списка вещей для оценки и
                                        предоставления выгодного предложения.</p>
                                    <div class="downloading-files">
                                        <div class="list">
                                            <div class="item">
                                                <div class="left-col document">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Создать список онлайн</h3>
                                                            <p>Найдите и добавьте вещи, которые необходимо перевезти</p>
                                                        </div>
                                                    </div>

                                                    <div class="right-col">
                                                        <div class="list-things-bt create_list_js">
                                                            <button>Создать список</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="item">
                                                <div class="left-col foto">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Загрузить фото</h3>
                                                            <p>Объем фото не должен превышать 5 Mb</p>
                                                        </div>
                                                    </div>
                                                    <div class="right-col">
                                                        <div class="list-things-bt open_foto">
                                                            <button>Прикрепить фото</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hover-downloading-foto">
                                                <div class="foto-block">
                                                    <div class="new-fot add-plus">
                                                        <label for="uploadbtn" class="uploadButton"></label>
                                                        <input style="opacity: 0; z-index: -1;" type="file"
                                                               name="upload" id="uploadbtn">
                                                    </div>
                                                    <div class="new-foto add-f">
                                                        <img src="img/main/block1/girl.png"
                                                             alt="">
                                                        <div class="del-img"></div>
                                                    </div>
                                                    <div class="new-foto add-f">
                                                        <img src="img/order-relocation/tabs3/box.png"
                                                             alt="">
                                                        <div class="del-img"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="list">
                                            <div class="item">
                                                <div class="left-col things">
                                                </div>
                                                <div class="all-coll">
                                                    <div class="center-col">
                                                        <div class="info">
                                                            <h3>Загрузить готовый список вещей</h3>
                                                            <p>Можно прикрепить файлы формата pdf/doc/xls.</p>
                                                        </div>
                                                    </div>

                                                    <div class="right-col">
                                                        <div class="list-things-bt open_download_file">
                                                            <button>Прикрепить документ</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hover-downloading-file">
                                                <div class="foto-block">
                                                    <p>Загружен файл</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="arrow-steps">
                                        <div class="but-btn">
                                            <button>Назад</button>
                                        </div>
                                        <div class="but-btn  choice_paragraph">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>


                                <div class="create-things">
                                    <p>Составление списка вещей способствует оптимизации переезда.</p>
                                    <p> Найдите и добавьте вещи для каждого имеющегося у Вас помещения.</p>
                                    <div class="catalog-things">
                                        <div class="choise-catalog-things">
                                            <div class="tab-menu-things">
                                                <button class="filter active">
                                                    Гостинная
                                                </button>
                                                <button class="filter">
                                                    Спальня
                                                </button>
                                                <button class="filter">
                                                    Столовая
                                                </button>
                                                <button class="filter">
                                                    Кухня
                                                </button>
                                                <button class="filter">
                                                    Ванная комната
                                                </button>
                                                <button class="filter">
                                                    Прихожая
                                                </button>
                                                <button class="filter">
                                                    Офис
                                                </button>
                                                <button class="filter">
                                                    Балкон/Мансарда/Терраса
                                                </button>
                                                <button class="filter">
                                                    Гараж/Подвал/Чердак
                                                </button>
                                                <button class="filter">
                                                    Кладовая
                                                </button>
                                                <button class="filter">
                                                    Другое
                                                </button>
                                            </div>
                                            <div class="tab-content-things">
                                                <div class="content-choice-things">
                                                    <div class="some-things">
                                                        <h3>Гостинная</h3>
                                                        <div class="delete-block clearfix">
                                                            <div class="delete-things">
                                                            </div>
                                                        </div>
                                                        <div class="information-things">
                                                            <div class="left-col">
                                                                <div class="l-col">
                                                                    <img src="img/order-relocation/tabs3/furniture/1.svg"
                                                                         alt="">
                                                                </div>
                                                                <div class="r-col">
                                                                    <h5>Книжная полка</h5>
                                                                    <p class="parametr_specify">0,86 <span> м3</span>
                                                                    </p>
                                                                    <div class="parametr">
                                                                        <p>Уточнить габаритные размеры</p>
                                                                    </div>
                                                                    <div class="specify-dimensions">

                                                                        <div class="col-dim">
                                                                            <label class="all-label">L, м</label>
                                                                            <input class="l-param" type="text" name="l"
                                                                            >
                                                                        </div>
                                                                        <div class="col-dim">
                                                                            <label class="all-label">H, м</label>
                                                                            <input class="h-param" type="text" name="h"
                                                                            >
                                                                        </div>
                                                                        <div class="col-dim">
                                                                            <label class="all-label">B, м</label>
                                                                            <input class="v-param" type="text" name="v"
                                                                            >
                                                                        </div>
                                                                        <div class="add-dim"></div>
                                                                        <div class="del-dim"></div>
                                                                    </div>
                                                                    <div class="counter">
                                                                        <span class="down"></span>
                                                                        <input type="text" value="1"/>
                                                                        <span class="up"></span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="right-col">
                                                                <div class="r-all">
                                                                    <label class="all-label">Комментарий:</label>
                                                                    <textarea class="popup-picking__text" rows="3"
                                                                              name="commit"
                                                                              placeholder="Комментарий"></textarea>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="all-foto-block">
                                                            <div class="add-foto">
                                                                <div class="butt">
                                                                    <a href="#" class="uploadButton">Добавить фото</a>
                                                                </div>
                                                                <div class="close-foto-things"></div>
                                                            </div>
                                                            <div class="arhive-foto">
                                                                <div class="foto-block">
                                                                    <div class="new-foto add-plus">
                                                                        <label for="uploadbtn"
                                                                               class="uploadButton"></label>
                                                                        <input style="opacity: 0; z-index: -1;"
                                                                               type="file"
                                                                               name="upload" id="uploadbtn">
                                                                    </div>
                                                                    <div class="new-foto add-f">
                                                                        <img src="img/order-relocation/tabs3/1.jpg"
                                                                             alt="">
                                                                        <div class="del-img"></div>
                                                                    </div>
                                                                    <div class="new-foto add-f">
                                                                        <img src="img/order-relocation/tabs3/box.png"
                                                                             alt="">
                                                                        <div class="del-img"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="line_shadow">
                                                        </div>
                                                    </div>


                                                    <div class="some-things untitled">
                                                        <div class="delete-block clearfix">
                                                            <div class="delete-things">
                                                            </div>
                                                        </div>
                                                        <div class="information-things">
                                                            <div class="left-col">
                                                                <div class="r-col">

                                                                    <div class="r-all">
                                                                        <label class="all-label">Название</label>
                                                                        <input class="popup-picking__text" rows="3"
                                                                               name="name-things"
                                                                               placeholder="Введите название"/>
                                                                    </div>
                                                                    <div class="specify-dimensions block-active-things">

                                                                        <div class="col-dim">
                                                                            <label class="all-label">L, м</label>
                                                                            <input class="l-param" type="text"
                                                                                   name="l"
                                                                            >
                                                                        </div>
                                                                        <div class="col-dim">
                                                                            <label class="all-label">H, м</label>
                                                                            <input class="h-param" type="text"
                                                                                   name="h"
                                                                            >
                                                                        </div>
                                                                        <div class="col-dim">
                                                                            <label class="all-label">B, м</label>
                                                                            <input class="v-param" type="text"
                                                                                   name="v"
                                                                            >
                                                                        </div>
                                                                        <div class="add-dim"></div>
                                                                        <div class="del-dim"></div>
                                                                    </div>
                                                                    <div class="counter not-name-count">
                                                                        <span class="down"></span>
                                                                        <input type="text" value="1"/>
                                                                        <span class="up"></span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="right-col">
                                                                <div class="r-all">
                                                                    <label class="all-label">Комментарий:</label>
                                                                    <textarea class="popup-picking__text" rows="3"
                                                                              name="commit"
                                                                              placeholder="Комментарий"></textarea>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="all-foto-block">
                                                            <div class="add-foto">
                                                                <div class="butt">
                                                                    <a href="#" class="uploadButton">Добавить фото</a>
                                                                </div>
                                                                <div class="close-foto-things"></div>
                                                            </div>
                                                            <div class="arhive-foto">
                                                                <div class="foto-block">
                                                                    <div class="new-foto add-plus">
                                                                        <label for="uploadbtn"
                                                                               class="uploadButton"></label>
                                                                        <input style="opacity: 0; z-index: -1;"
                                                                               type="file" name="upload" id="uploadbtn">
                                                                    </div>
                                                                    <div class="new-foto add-f">
                                                                        <img src="img/order-relocation/tabs3/1.jpg"
                                                                             alt="">
                                                                        <div class="del-img"></div>
                                                                    </div>
                                                                    <div class="new-foto add-f">
                                                                        <img src="img/order-relocation/tabs3/box.png"
                                                                             alt="">
                                                                        <div class="del-img"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="line_shadow">
                                                        </div>
                                                    </div>
                                                    <div class="but-btn  add_things_js">
                                                        <button>ДОбавить</button>
                                                    </div>
                                                    <div class="other-things">
                                                        <div class="things">
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/1.svg"
                                                                     alt="">
                                                                <p>Книжная полка</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/6Sideboard.svg"
                                                                     alt="">
                                                                <p>Сервант</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/11TV.svg"
                                                                     alt="">
                                                                <p>Телевизор</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/16Plant.svg"
                                                                     alt="">
                                                                <p>Вазон</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                                     alt="">
                                                                <p>Стол</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/7Buffet.svg"
                                                                     alt="">
                                                                <p>Буфет</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/12Stereo.svg"
                                                                     alt="">
                                                                <p>Стерео</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/17Box.svg"
                                                                     alt="">
                                                                <p>Коробка</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                                     alt="">
                                                                <p>Диван</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/8Cupboard.svg"
                                                                     alt="">
                                                                <p>Тумба</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                                     alt="">
                                                                <p>Картина</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                                     alt="">
                                                                <p>Кресло</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/9Cupboard.svg"
                                                                     alt="">
                                                                <p>Шкаф</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/14Chandelier.svg"
                                                                     alt="">
                                                                <p>Люстра</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/5Sideboard.svg"
                                                                     alt="">
                                                                <p>Стул</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/10Carpet.svg"
                                                                     alt="">
                                                                <p>Ковер</p>
                                                            </div>
                                                            <div class="item">
                                                                <img class="svg"
                                                                     src="img/order-relocation/tabs3/furniture/15Floor_lamp.svg"
                                                                     alt="">
                                                                <p>Торшер</p>
                                                            </div>
                                                            <div class="item other-add">
                                                                <img src="img/order-relocation/tabs3/add.png" alt="">
                                                                <a href="#">Другое</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-choice-things">
                                                    <!-- <p id="person"></p>-->
                                                    <!-- <div id="templates" style="display: none;"></div>-->
                                                    333
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="arrow-steps">
                                        <div class="but-btn">
                                            <button>Назад</button>
                                        </div>
                                        <div class="but-btn  choice_paragraph">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>


                                <div class="check-list complete_checking_things">
                                    <div class="form-group form-all">
                                        <h4>Полный список Ваших вещей для переезда</h4>
                                        <p>Убедитесь, что в данном списке учтены все вещи, которые необходимо перевезти.
                                            Сумма предложения от исполнителя формируется на основании этого списка.
                                            В случае, если объем или количество вещей будут отличаться, это может
                                            повлиять
                                            на конечную стоимость услуги.</p>
                                    </div>
                                    <div class="completed_things">
                                        <h2>Гостинная</h2>
                                        <div class="list-things">
                                            <div class="item">
                                                <div class="l-col">
                                                    <img src="img/order-relocation/tabs3/furniture/1.svg"
                                                         alt="">
                                                </div>
                                                <div class="center-col">
                                                    <h5>Книжная полка <span></span></h5>
                                                    <p class="parametr_specify">0,86 <span> м3</span>
                                                    </p>
                                                    <p>1х2х0,4 <span> м</span></p>
                                                </div>
                                                <div class="r-col">
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="l-col">
                                                    <img src="img/order-relocation/tabs3/furniture/2Table.svg"
                                                         alt="">
                                                </div>
                                                <div class="center-col">
                                                    <h5>Стол <span> x 5</span></h5>
                                                    <p class="parametr_specify">0,86 <span> м3</span>
                                                    </p>
                                                </div>
                                                <div class="r-col">
                                                    <p>Переносить аккуратно - раритет</p>
                                                    <a href="#js_popup_slider_foto">Смотреть фото</a>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="completed_things">
                                        <h2>Кухня</h2>
                                        <div class="list-things">
                                            <div class="item">
                                                <div class="l-col">
                                                    <img src="img/order-relocation/tabs3/furniture/3Sofa.svg"
                                                         alt="">
                                                </div>
                                                <div class="center-col">
                                                    <h5>Коробка <span> x 5</span></h5>
                                                    <p class="parametr_specify">0,86 <span> м3</span>
                                                    </p>
                                                    <p>1х2х0,4 <span> м</span></p>
                                                </div>
                                                <div class="r-col">
                                                    <p>Коробки с посудой, фамильный сервиз - переносить осторожно</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="l-col">
                                                    <img src="img/order-relocation/tabs3/furniture/4Armchair.svg"
                                                         alt="">
                                                </div>
                                                <div class="center-col">
                                                    <h5>Стол <span> x 5</span></h5>
                                                    <p class="parametr_specify">0,86 <span> м3</span>
                                                    </p>
                                                </div>
                                                <div class="r-col">
                                                    <a href="#js_popup_slider_foto">Смотреть фото</a>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="completed_things">
                                        <h2>Прихожая</h2>
                                        <div class="list-things">
                                            <div class="item">
                                                <div class="l-col">
                                                    <img src="img/order-relocation/tabs3/furniture/13Picture.svg"
                                                         alt="">
                                                </div>
                                                <div class="center-col">
                                                    <h5>Коробка <span> x 5</span></h5>
                                                    <p class="parametr_specify">0,86 <span> м3</span>
                                                    </p>
                                                    <p>1х2х0,4 <span> м</span></p>
                                                </div>
                                                <div class="r-col">
                                                    <p>Коробки с посудой, фамильный сервиз - переносить осторожно</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="l-col">
                                                    <img src="img/order-relocation/tabs3/furniture/18Zaglushka.svg"
                                                         alt="">
                                                </div>
                                                <div class="center-col">
                                                    <h5>Стол <span> x 5</span></h5>
                                                    <p class="parametr_specify">0,86 <span> м3</span>
                                                    </p>
                                                </div>
                                                <div class="r-col">

                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="all-quantity-things">
                                        <h2>Всего <span class="quantity-things">32,68</span> <span>м3</span></h2>
                                    </div>


                                    <div class="arrow-steps">
                                        <div class="but-btn btnChangeThings">
                                            <button>Изменить</button>
                                        </div>
                                        <div class="but-btn  btnNext">
                                            <button>Далее</button>
                                        </div>
                                    </div>


                                </div>


                                <div class="check-list full_photo_check">
                                    <div class="form-group form-all">
                                        <h4>Полный список Ваших вещей для переезда</h4>
                                        <p>Убедитесь, что в данном списке учтены все вещи, которые необходимо перевезти.
                                            Сумма предложения от исполнителя формируется на основании этого списка.
                                            В случае, если объем или количество вещей будут отличаться, это может
                                            повлиять
                                            на конечную стоимость услуги.</p>
                                    </div>
                                    <div class="completed_things">
                                        <h2>Загруженные фото</h2>
                                        <div class="list-things">
                                            <div class="compl_foto">

                                                <div class="item">
                                                    <img src="img/order-relocation/tabs3/foto_download/1.png" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/order-relocation/tabs3/foto_download/2.png" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/order-relocation/tabs3/foto_download/3.png" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/order-relocation/tabs3/foto_download/4.png" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/order-relocation/tabs3/foto_download/7.png" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/order-relocation/tabs3/foto_download/5.png" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/order-relocation/tabs3/foto_download/6.png" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/order-relocation/tabs3/foto_download/5.png" alt="">
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="arrow-steps">
                                        <div class="but-btn btnChangeThings">
                                            <button>Изменить</button>
                                        </div>
                                        <div class="but-btn  choice_paragraph">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="check-list full_download_check">
                                    <div class="form-group form-all">
                                        <h4>Полный список Ваших вещей для переезда</h4>
                                        <p>Убедитесь, что в данном списке учтены все вещи, которые необходимо перевезти.
                                            Сумма предложения от исполнителя формируется на основании этого списка.
                                            В случае, если объем или количество вещей будут отличаться, это может
                                            повлиять
                                            на конечную стоимость услуги.</p>
                                    </div>
                                    <div class="completed_things">
                                        <h2>Готовый список вещей</h2>
                                        <div class="download_file">

                                        </div>
                                    </div>

                                    <div class="arrow-steps">
                                        <div class="but-btn btnChangeThings">
                                            <button>Изменить</button>
                                        </div>
                                        <div class="but-btn  choice_paragraph">
                                            <button>Далее</button>
                                        </div>
                                    </div>
                                </div>
<!--                                <div class="arrow-steps">-->
<!--                                    <div class="but-btn btnChangeThings">-->
<!--                                        <button>Изменить</button>-->
<!--                                    </div>-->
<!--                                    <div class="but-btn  choice_paragraph">-->
<!--                                        <button>Далее</button>-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                        </div>
                        <div class="content-choice four-st">
                            <div class="center-block">
                                <div class="geography-block">
                                    <div class="order-lk form-group form-all">
                                        <p>Укажите, пожалуйста, Ваши контактные данные. В ближайшее время на адрес Вашей электронной почты будет отправлено письмо со ссылкой, по которой Вы сможете просмотреть полученные предложения.</p>
                                        <h4>Укажите Ваши контактные данные</h4>
                                        <div class="recourse-lk contact-things">
                                            <div class="recourse-name">
                                                <div class="item">
                                                    <label>
                                                        <input class="radio" type="radio" name="radio-test" value="Господин" checked>
                                                        <span class="radio-custom"></span>
                                                        <span class="label">Господин</span>
                                                    </label>
                                                </div>
                                                <div class="item">
                                                    <label>
                                                        <input class="radio" type="radio" name="radio-test" value="Госпожа">
                                                        <span class="radio-custom"></span>
                                                        <span class="label">Госпожа</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="all-profile lk-rel">
                                                <div class="cont-things-form">
                                                    <div class="item">
                                                        <input type="text" name="name" placeholder="Ваше имя">
                                                    </div>
                                                    <div class="item">
                                                        <input type="text" name="surname" placeholder="Фамилия">
                                                    </div>
                                                    <div class="item">
                                                        <input  type="tel" name="email" placeholder="Email">
                                                    </div>
                                                    <div class="item">
                                                        <input class="form-control"  placeholder="Телефон" name="phone" id="Piramida_phone" type="tel" maxlength="20" />
                                                        <div class="help-block error" id="Piramida_phone_em_" style="display:none"></div>
                                                        <input type="checkbox" id="phone_mask" placeholder="Телефон" checked>
                                                        <label id="descr" for="phone_mask" class="phone-mask">Использовать маску</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="arrow-steps">
                                        <div class="but-btn btnPrevious">
                                            <button>Назад</button>
                                        </div>
                                        <div class="but-btn">
                                            <button>Отправить заявку</button>
                                        </div>
                                    </div>


                                    </div>


                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    </div>
</div>



<script type="text/javascript" src="/js/lk_order/steps_relocation.js"></script>
<script type="text/javascript" src="/js/lk_order/create_things.js"></script>
<script type="text/javascript" src="/js/lk_order/popup_slider_foto.js"></script>
<script type="text/javascript" src="/js/lk_order/things.js"></script>
<?php include("template/popUps.php") ?>
<?php include("template/footer.php") ?>
